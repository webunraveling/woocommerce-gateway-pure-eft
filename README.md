# woocommerce-pure-eft
A WooCommerce payment gateway plugin for WordPress. It accepts account information from users for you to process Electronic Funds Transfer (EFT) payments.

A bank account number, routing number and type of account are requested during checkout. This information is then saved to the order (optionally sent in admin email too). You can then initiate an EFT with your bank using this information.

# Donate
To help me continue working on this plugin, consider making a donation. It would make me so happy!

If you found this useful, please buy me a cup of tea:

[![Donate with PayPal](http://webunraveling.com/public/business/donate/paypal-button-200px.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LJAT6WCWJCQUW) <a href="https://webunraveling.com/public/business/donate/pure-eft.html" target="_blank" rel="noopener nofollow"><img src="https://webunraveling.com/public/business/donate/donate-button-crypto.png" /></a>
